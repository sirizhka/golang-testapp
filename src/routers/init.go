package routers

import (
	"app"
	"github.com/gorilla/mux"
	"middleware"
	"net/http"
)

var Router *mux.Router

func init() {
	router := mux.NewRouter()

	httpRouter := router.PathPrefix("/api").Subrouter()
	wsRouter := router.PathPrefix("/ws").Subrouter()

	unauthRouter := httpRouter.PathPrefix("/").Subrouter()
	authRouter := httpRouter.PathPrefix("/auth").Subrouter()

	unauthRouter.HandleFunc("/", app.HomeHandler).Methods("GET")
	unauthRouter.HandleFunc("/login", app.LoginHandler).Methods("POST")
	unauthRouter.HandleFunc("/registration", app.RegisterHandler).Methods("POST")

	authRouter.Use(middleware.AuthMiddleware)
	authRouter.HandleFunc("/info", app.InfoHandler).Methods("GET")

	wsRouter.Use(middleware.AuthMiddleware)
	wsRouter.HandleFunc("/test", app.WSHandler)

	httpRouter.Use(middleware.HttpLoggingMiddleware)
	wsRouter.Use(middleware.WsLoggingMiddleware)
	router.NotFoundHandler = http.HandlerFunc(app.NotFoundHandler)

	Router = router
}
