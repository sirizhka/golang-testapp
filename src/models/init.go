package models

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
)

const (
	user     = "postgres"
	host     = "localhost"
	port     = "5432"
	dbname   = "victoriner"
	password = ""
)

var db *sql.DB

func init() {
	cfg := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s",
		host, port, user, dbname, password)
	conn, err := sql.Open("postgres", cfg)
	if err != nil {
		log.Println(err)
		return
	}

	err = conn.Ping()
	if err != nil {
		log.Println(err)
		return
	}

	db = conn
}

func GetDB() *sql.DB {
	return db
}
