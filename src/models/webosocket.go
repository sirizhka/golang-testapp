package models

import "github.com/gorilla/websocket"

type ClientConnect struct {
	Conn     *websocket.Conn
	Nickname string
}

var ClientsPool = make(map[ClientConnect]bool, 0)
