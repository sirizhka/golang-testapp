package models

import (
	"golang.org/x/crypto/bcrypt"
	"log"
)

type User struct {
	Id       int    `json:"_id"`
	Nickname string `json:"nickname"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

func HashAndSalt(pswd string) string {
	hash, err := bcrypt.GenerateFromPassword([]byte(pswd), bcrypt.MinCost)
	if err != nil {
		log.Println(err)
	}
	return string(hash)
}
