package main

import (
	"log"
	"net/http"
	"routers"
	"time"
)

func main() {
	server := http.Server{
		Handler:      routers.Router,
		Addr:         ":8080",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	log.Printf("Server started work at %v", server.Addr)
	log.Fatal(server.ListenAndServe())
}
