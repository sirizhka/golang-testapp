package app

import (
	"database/sql"
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
	"models"
	"net/http"
	"time"
	"utils"
)

var SigningKey = []byte("$c2q3=39*)q5f9415rl5mn!m+j)24@$%_-bd^)e9ic&0p)&5xb")

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	var credentials models.Credentials
	var response models.BaseResponse

	err := json.NewDecoder(r.Body).Decode(&credentials)
	if err != nil {
		response.Error = "Bad request"
		response.Status = false
		w.WriteHeader(http.StatusBadRequest)
		utils.Respond(w, r, response)
		return
	}
	defer r.Body.Close()

	db := models.GetDB()
	var nickname, password string
	res := db.QueryRow("select nickname, password from users_user where nickname = $1", credentials.Nickname).
		Scan(&nickname, &password)
	if res == sql.ErrNoRows {
		response.Error = "Credentials not found"
		response.Status = false
		w.WriteHeader(http.StatusUnauthorized)
		utils.Respond(w, r, response)
		return
	}
	if bcrypt.CompareHashAndPassword([]byte(password), []byte(credentials.Password)) != nil {
		response.Error = "Password did not match"
		response.Status = false
		w.WriteHeader(http.StatusUnauthorized)
		utils.Respond(w, r, response)
		return
	}

	expirationTime := time.Now().Add(time.Hour * 24)
	claims := &models.Claims{
		Nickname: credentials.Nickname,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenStr, err := token.SignedString(SigningKey)
	if err != nil {
		response.Error = "Some problems"
		response.Status = false
		w.WriteHeader(http.StatusBadRequest)
		utils.Respond(w, r, response)
		return
	}

	response.Status = true
	response.Data = map[string]string{
		"token": tokenStr,
	}
	utils.Respond(w, r, response)
}
