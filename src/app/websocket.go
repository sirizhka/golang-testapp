package app

import (
	"github.com/gorilla/websocket"
	"log"
	"models"
	"net/http"
	"time"
)

var upgrader = websocket.Upgrader{
	WriteBufferSize:  1024,
	ReadBufferSize:   1024,
	HandshakeTimeout: 15 * time.Second,
}

func WSHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	user := r.Context().Value("user").(*models.Claims)
	clientConnect := models.ClientConnect{
		Nickname: user.Nickname,
		Conn:     conn,
	}
	models.ClientsPool[clientConnect] = true
}
