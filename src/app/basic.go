package app

import (
	"encoding/json"
	"log"
	"models"
	"net/http"
	"time"
	"utils"
)

func RegisterHandler(w http.ResponseWriter, r *http.Request) {
	var user models.User
	var response models.BaseResponse

	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		response.Error = "Bad request"
		response.Status = false
		w.WriteHeader(http.StatusBadRequest)
		utils.Respond(w, r, response)
		return
	}
	defer r.Body.Close()

	db := models.GetDB()
	row := db.QueryRow("select count(id) from users_user")
	var id int
	err = row.Scan(&id)
	if err != nil {
		log.Fatal(err)
		return
	}

	pwd := models.HashAndSalt(user.Password)
	_, err = db.Exec("insert into users_user "+
		"(password, last_login, is_superuser, first_name, last_name, mid_name, email, phone, date_joined, is_active, is_staff, is_logged, age, sex, nickname) "+
		"values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15)",
		pwd, time.Now(), false, "", "", "", user.Email, "", time.Now(), true, false, false, 0, "", user.Nickname)
	if err != nil {
		log.Println(err)
		return
	}

	response.Status = true
	utils.Respond(w, r, response)
}

func NotFoundHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("%6v %15v not found", r.Method, r.URL)
	response := models.BaseResponse{
		Status: false,
		Error:  "Not found",
	}
	utils.Respond(w, r, response)
}

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	response := models.BaseResponse{
		Data:   "Home page",
		Status: true,
	}
	utils.Respond(w, r, response)
}
