package app

import (
	"fmt"
	"log"
	"models"
	"time"
)

func init() {
	go SendToClients()
}

func SendToClients() {
	var response models.BaseResponse
	for {
		for client, ok := range models.ClientsPool {
			data := map[string]string{
				"ok":       fmt.Sprintf("%v", ok),
				"nickname": client.Nickname,
			}
			response.Status = true
			response.Data = data

			err := client.Conn.WriteJSON(response)
			if err != nil {
				_ = client.Conn.Close()
				delete(models.ClientsPool, client)
				log.Printf("client: %v disconnected", client)
			}
		}
		time.Sleep(5 * time.Second)
	}
}
